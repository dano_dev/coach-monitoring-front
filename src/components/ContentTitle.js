import React from 'react';

const ContentTitle = ({text}) => {
  return <div className="content-title">{text}</div>;
};

export default ContentTitle;
