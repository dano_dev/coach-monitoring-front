import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Card } from 'semantic-ui-react';
import { API } from '../utils/misc';
import AlertModal from './AlertModal';
import LoaderComponent from '../components/LoaderComponent';
import ContentTitle from '../components/ContentTitle';

const StatisticCard = ({ link, value, title }) => {
  if (link) {
    return (
      <Card as={Link} to={link}>
        <Card.Content>
          <Card.Header>
            <h2>{value}</h2>
          </Card.Header>
          <Card.Meta>{title}</Card.Meta>
        </Card.Content>
      </Card>
    );
  } else {
    return (
      <Card>
        <Card.Content>
          <Card.Header>
            <h2>{value}</h2>
          </Card.Header>
          <Card.Meta>{title}</Card.Meta>
        </Card.Content>
      </Card>
    );
  }
};

const Dashboard = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(false);
  const [showErrorModal, setShowErrorModal] = useState(false);
  const [data, setData] = useState([
    {
      text: '어제 코칭타임 미준수 코치',
      value: 0,
      link: '/page-1',
      key: 'violatedCnt',
    },
    {
      text: '어제 코칭타임 미배정 코치',
      value: 0,
      link: '/page-2',
      key: 'unassignedCnt',
    },
    {
      text: '코칭타임 변경 히스토리',
      value: 0,
      link: '/page-3',
      key: 'changeCnt',
    },
    { text: '활동코치 수', value: 0 },
    { text: '휴직코치 수', value: 0 },
    { text: '수습코치 수', value: 0 },
  ]);

  const getDashboardData = async () => {
    setIsLoading(true);
    try {
      console.log('env:::', process.env.REACT_APP_SERVER_URL);
      const response = await API.get('/dashboard');
      console.log('getDashboardData response : ', response);
      console.log('getDashboardData data : ', data);
      if (response.data.error) throw response.data.error;
      data.forEach((x) => {
        if (x.key in response.data.data) {
          x.value = response.data.data[x.key];
        }
      });

    } catch (e) {
      console.log('getDashboardData : ', e);
      setShowErrorModal(true);
      setError(e.message);
    }
    setIsLoading(false);
  };

  useEffect(() => {
    getDashboardData();
  }, []);

  return (
    <div>
      <div className="content-header">
        <ContentTitle text="코칭타임 모니터링" />
      </div>
      <br />

      <Card.Group itemsPerRow={3} className="content content-statistic">
        {showErrorModal ? (
          <AlertModal
            title="error"
            message={error}
            setShowErrorModal={setShowErrorModal}
          />
        ) : null}
        <LoaderComponent isLoading={isLoading} />
        {data.map((item, idx) => (
          <StatisticCard
            link={item.link}
            value={item.value}
            title={item.text}
            key={`statisticcard` + idx}
          />
        ))}
      </Card.Group>

      <br />
      <br />
      <br />

      <div className="content-header">
        <ContentTitle text="코치 채팅 모니터링(Coming soon..)" />
      </div>
    </div>
  );
};

export default Dashboard;
