import React, { useState, useEffect } from 'react';
import ContentTitle from './ContentTitle';
import { Table } from 'semantic-ui-react';
import 'react-semantic-ui-datepickers/dist/react-semantic-ui-datepickers.css';
import { API, yesterdayDate } from '../utils/misc';
import LoaderComponent from '../components/LoaderComponent';
import AlertModal from './AlertModal';

const TableRow = ({ rowNo, myid, coachingName, unassignedCount }) => {
  return (
    <Table.Row>
      <Table.Cell>
        <p>{rowNo + 1}</p>
      </Table.Cell>
      <Table.Cell>
        <p>{myid}</p>
      </Table.Cell>
      <Table.Cell>
        <p>{coachingName}</p>
      </Table.Cell>
      <Table.Cell>
        <p>{unassignedCount}</p>
      </Table.Cell>
    </Table.Row>
  );
};

const UnassignedList = () => {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(false);
  const [showErrorModal, setShowErrorModal] = useState(false);

  const getUnassignedList = async () => {
    setIsLoading(true);
    try {
      const response = await API.get(
        `/get_unassigned_list/?date=${yesterdayDate}`,
      );
      setData(response.data.data);
      if (response.data.error) throw response.data;
    } catch (e) {
      setShowErrorModal(true);
      setError(e.message);
    }
    setIsLoading(false);
  };

  useEffect(() => {
    getUnassignedList();
  }, []);

  return (
    <div>
      {showErrorModal ? (
        <AlertModal
          title="error"
          message={error}
          setShowErrorModal={setShowErrorModal}
        />
      ) : null}
      <LoaderComponent isLoading={isLoading} />
      <div className="content-header">
        <ContentTitle text="어제 코칭타임 미배정 코치" />
      </div>
      <Table singleLine selectable className="list-table">
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>NO</Table.HeaderCell>
            <Table.HeaderCell>코치 ID</Table.HeaderCell>
            <Table.HeaderCell>코치명</Table.HeaderCell>
            <Table.HeaderCell>미배정 수강생수</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {data.map((item, idx) => (
            <TableRow
              rowNo={idx}
              myid={item.myid}
              coachingName={item.name}
              unassignedCount={item.count}
              key={'unassignedListTableRow' + idx}
            />
          ))}
        </Table.Body>
      </Table>
    </div>
  );
};

export default UnassignedList;
