import React, { useState, useEffect } from 'react';
import ContentTitle from './ContentTitle';
import _ from 'lodash';
import { timeParser, validateDate } from '../utils/misc';
import { Table, Input, Label } from 'semantic-ui-react';
import SemanticDatepicker from 'react-semantic-ui-datepickers';
import 'react-semantic-ui-datepickers/dist/react-semantic-ui-datepickers.css';
import { API, labelColor, yesterdayDate } from '../utils/misc';
import moment from 'moment';
import LoaderComponent from '../components/LoaderComponent';
import AlertModal from './AlertModal';
import TidListModal from './TidListModal';

const ViolatedList = () => {
  const [data, setData] = useState([]);
  const [tidList, setTidList] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [searchKeyword, setSearchKeyword] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(false);
  const [showTidListModal, setShowTidListModal] = useState(false);
  const [showErrorModal, setShowErrorModal] = useState(false);
  const [targetDate, setTargetDate] = useState(yesterdayDate);
  const [selectedGroupInfo, setSelectedGroupInfo] = useState({});

  const TableRow = ({ rowNo, coach, violationDetail, violatedCntTotal }) => {
    return (
      <Table.Row>
        <Table.Cell>
          <div>
            <p>{rowNo + 1}</p>
          </div>
        </Table.Cell>
        <Table.Cell>
          <div>
            <p>{coach.myid}</p>
          </div>
        </Table.Cell>
        <Table.Cell>
          <div>
            <p>{coach.name}</p>
          </div>
        </Table.Cell>
        <Table.Cell>
          <div>
            {violationDetail.map((item, idx) => (
              <div key={`violationinfo` + idx}>
                <Label
                  circular
                  size="mini"
                  basic
                  color={labelColor(item.programTypeId)}
                  className="program-type-label"
                >
                  {item.programTypeId === 0 ? '토탈 ' : '베이직 '} {item.groupId}
                </Label>
                <span className="group-name">{item.groupName}</span>{' '}
                {timeParser(item.coachingTime)}
              </div>
            ))}
          </div>
        </Table.Cell>
        <Table.Cell>
          <div className="affectedcnt">
            {violationDetail.map((item, idx) => (
              <div
                className="afftected-cnt"
                onClick={() => openTidListModal(item, targetDate)}
              >
                <span>{item.affectedCnt}</span>
              </div>
            ))}
          </div>
        </Table.Cell>
        <Table.Cell>
          <div>
            <p>{violationDetail.length}</p>
          </div>
        </Table.Cell>
        <Table.Cell>
          <div>
            <p>{violatedCntTotal}</p>
          </div>
        </Table.Cell>
      </Table.Row>
    );
  };

  const getViolatedList = async (date) => {
    console.log('date', date);
    const dateTimeValidation = validateDate(date);
    console.log(dateTimeValidation);
    if (!dateTimeValidation) return;

    if (!date) return;
    setIsLoading(true);
    try {
      const response = await API.get(`/get_violated_list/?date=${date}`);
      setData(response.data.data);
      setFilteredData(response.data.data);
      setSearchKeyword('');
      if (response.data.error) throw response.data;
    } catch (e) {
      setShowErrorModal(true);
      setError(e.message);
    }
    setIsLoading(false);
  };

  const getTidList = async (groupId, targetDate) => {
    setIsLoading(true);
    try {
      const response = await API.get(
        `/get_tid_list/?date=${targetDate}&group_id=${groupId}`,
      );
      if (response.data.error) throw response.data;
      setTidList(response.data.data);
    } catch (e) {
      setShowErrorModal(true);
      setError(e.message);
    }
    setIsLoading(false);
  };

  const openTidListModal = async (groupInfo, targetDate) => {
    await getTidList(groupInfo.groupId, targetDate);
    setSelectedGroupInfo(groupInfo);
    setShowTidListModal(true);
  };

  const changeTargetDate = (event, date) => {
    const formattedDate = moment(date.value).format('YYYY-MM-DD');
    getViolatedList(formattedDate);
    setTargetDate(formattedDate);
  };

  const handleSearchChange = (e, { value }) => {
    setFilteredData(
      data.filter(
        (x) => x.coach.name.includes(value) || x.coach.myid.includes(value),
      ),
    );
    setSearchKeyword(value);
  };

  useEffect(() => {
    getViolatedList(yesterdayDate);
  }, []);

  return (
    <div>
      {showTidListModal ? (
        <TidListModal
          groupInfo={selectedGroupInfo}
          tidList={tidList}
          targetDate={targetDate}
          setShowTidListModal={setShowTidListModal}
        />
      ) : null}

      {showErrorModal ? (
        <AlertModal
          title="error"
          message={error}
          setShowErrorModal={setShowErrorModal}
        />
      ) : null}
      <LoaderComponent isLoading={isLoading} />
      <div className="content-header">
        <ContentTitle text={`${targetDate} 코칭타임 미준수 코치`} />
        <Input onChange={handleSearchChange} placeholder="코치명, ID로 검색" />
        <SemanticDatepicker onChange={changeTargetDate} clearIcon={false} />
      </div>
      <Table singleLine selectable className="list-table violated-list-table">
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>NO</Table.HeaderCell>
            <Table.HeaderCell>코치 ID</Table.HeaderCell>
            <Table.HeaderCell>코치명</Table.HeaderCell>
            <Table.HeaderCell>미준수 코칭타임</Table.HeaderCell>
            <Table.HeaderCell>영향받은 수강생 수</Table.HeaderCell>
            <Table.HeaderCell>미준수 횟수</Table.HeaderCell>
            <Table.HeaderCell>이번달 누적</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {!_.isEmpty(filteredData) ? (
            filteredData.map((item, idx) => (
              <TableRow
                rowNo={idx}
                coach={item.coach}
                violationDetail={item.violationDetail}
                violatedCntTotal={item.violatedCntTotal}
                key={'violatedListTableRow' + idx}
              />
            ))
          ) : (
            <Table.Row>
              <Table.Cell colSpan="7" className="empty-table">
                해당일의 데이터가 존재하지 않습니다.
              </Table.Cell>
            </Table.Row>
          )}
        </Table.Body>
      </Table>
    </div>
  );
};

export default ViolatedList;
