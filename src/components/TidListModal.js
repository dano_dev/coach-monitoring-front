import React, { useEffect, useState } from 'react';
import { Button, Modal } from 'semantic-ui-react';
import { API, timeParser, getProgramTypeStr } from '../utils/misc';

const TidListModal = ({
  groupInfo,
  targetDate,
  tidList,
  setShowTidListModal,
}) => {
  return (
    <Modal open={true}>
      <Modal.Header>
        {targetDate} {groupInfo.groupName} 그룹 내 영향받은 수강생 리스트(총{' '}
        {groupInfo.affectedCnt}명)
        <div className="modal-header-desc">
          그룹ID: {groupInfo.groupId} / 코칭타임:{' '}
          {timeParser(groupInfo.coachingTime)} / 프로그램 타입:{' '}
          {getProgramTypeStr(groupInfo.programTypeId)}
        </div>
      </Modal.Header>
      <Modal.Content scrolling>
        <Modal.Description>
          <ul>
            {tidList.map((tid) => (
              <li>
                <a
                  href={`https://dietnote.net/coach/user/${tid.userId}`}
                  target="_blank"
                >
                  {tid.name} ({tid.myid})
                </a>
              </li>
            ))}
          </ul>
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
        <Button
          primary
          content="닫기"
          onClick={() => setShowTidListModal(false)}
        />
      </Modal.Actions>
    </Modal>
  );
};

export default TidListModal;
