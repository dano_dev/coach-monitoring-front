import React from 'react';
import { Button, Modal } from 'semantic-ui-react';

const AlertModal = ({ message, setShowErrorModal }) => {
  return (
    <Modal open={true}>
      <Modal.Content>
        <Modal.Description>{message}</Modal.Description>
      </Modal.Content>
      <Modal.Actions>
        <Button
          primary
          content="닫기"
          onClick={() => setShowErrorModal(false)}
        />
      </Modal.Actions>
    </Modal>
  );
};

export default AlertModal;
