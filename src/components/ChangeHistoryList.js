import React, { useState, useEffect } from 'react';
import ContentTitle from './ContentTitle';
import { timeParser, yesterdayDate } from '../utils/misc';
import { Table, Label, Input } from 'semantic-ui-react';
import SemanticDatepicker from 'react-semantic-ui-datepickers';
import 'react-semantic-ui-datepickers/dist/react-semantic-ui-datepickers.css';
import { API, labelColor } from '../utils/misc';
import LoaderComponent from '../components/LoaderComponent';
import * as moment from 'moment-timezone';
import AlertModal from './AlertModal';

const TableRow = ({ rowNo, coach, changeHistoryDetail }) => {
  return (
    <Table.Row>
      <Table.Cell>
        <div>
          <p>{rowNo + 1}</p>
        </div>
      </Table.Cell>
      <Table.Cell>
        <div>
          <p>{coach.myid}</p>
        </div>
      </Table.Cell>
      <Table.Cell>
        <div>
          <p>{coach.name}</p>
        </div>
      </Table.Cell>
      <Table.Cell>
        <div>
          {changeHistoryDetail.map((item, idx) => (
            <p key={`validFrom` + idx}>
              {moment(item.validFrom).tz('Asia/Seoul').format('MM월 DD일')}
            </p>
          ))}
        </div>
      </Table.Cell>
      <Table.Cell>
        <div>
          {changeHistoryDetail.map((item, idx) => (
            <div key={`groupName` + idx}>
              <Label
                circular
                size="mini"
                basic
                color={labelColor(item.programTypeId)}
                className="program-type-label"
              >
                ID:{item.groupId}
              </Label>
              <span className="group-name">{item.groupName}</span>
              {timeParser(item.coachingTime)}
            </div>
          ))}
        </div>
      </Table.Cell>
      <Table.Cell>
        <div>
          {changeHistoryDetail.map((item, idx) => (
            <p key={'programTypelabel' + idx}>{item.affectedCnt}</p>
          ))}
        </div>
      </Table.Cell>
      <Table.Cell>
        <div>{changeHistoryDetail.length}</div>
      </Table.Cell>
    </Table.Row>
  );
};

const ChangeHistoryList = () => {
  const [data, setData] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [searchKeyword, setSearchKeyword] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(false);
  const [showErrorModal, setShowErrorModal] = useState(false);
  const [targetDate, setTargetDate] = useState([yesterdayDate, yesterdayDate]);

  const getChangedHistory = async (start, end) => {
    setSearchKeyword('');
    setIsLoading(true);
    try {
      const response = await API.get(
        `/get_change_history/?start=${start}&end=${end}`,
      );
      setData(response.data.data);
      setFilteredData(response.data.data);
      setSearchKeyword('');
      if (response.data.error) throw response.data;
    } catch (e) {
      setShowErrorModal(true);
      setError(e.message);
    }
    setIsLoading(false);
  };

  const changeTargetDate = (event, date) => {
    if (date.value.length < 2) return;
    const formattedDateStart = moment(date.value[0]).format('YYYY-MM-DD');
    const formattedDateEnd = moment(date.value[1]).format('YYYY-MM-DD');

    getChangedHistory(formattedDateStart, formattedDateEnd);
    setTargetDate([formattedDateStart, formattedDateEnd]);
  };

  const handleSearchChange = (e, { value }) => {
    setFilteredData(
      data.filter(
        (x) => x.coach.name.includes(value) || x.coach.myid.includes(value),
      ),
    );
    setSearchKeyword(value);
  };

  const getDateToDisplay = (targetDate) => {
    return targetDate[0] === targetDate[1]
      ? targetDate[0]
      : `${targetDate[0]}~${targetDate[1]}`;
  };

  useEffect(() => {
    getChangedHistory(yesterdayDate, yesterdayDate);
  }, []);

  return (
    <div className="change-history-list">
      {showErrorModal ? (
        <AlertModal
          title="error"
          message={error}
          setShowErrorModal={setShowErrorModal}
        />
      ) : null}
      <LoaderComponent isLoading={isLoading} />
      <div className="content-header">
        <ContentTitle
          text={`${getDateToDisplay(targetDate)} 코칭타임 변경 히스토리`}
        />
        <Input onChange={handleSearchChange} placeholder="코치명, ID로 검색" />
        <SemanticDatepicker type="range" onChange={changeTargetDate} clearIcon={false} />
      </div>
      <Table singleLine selectable className="list-table">
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>NO</Table.HeaderCell>
            <Table.HeaderCell>코치 ID</Table.HeaderCell>
            <Table.HeaderCell>코치명</Table.HeaderCell>
            <Table.HeaderCell>변경일</Table.HeaderCell>
            <Table.HeaderCell>코칭타임 그룹</Table.HeaderCell>
            <Table.HeaderCell>그룹 내 수강생수</Table.HeaderCell>
            <Table.HeaderCell>선택 기간 내 변경 횟수</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {filteredData.map((item, idx) => (
            <TableRow
              rowNo={idx}
              coach={item.coach}
              changeHistoryDetail={item.changeHistoryDetail}
              key={'ChangeHistoryTableRow' + idx}
            />
          ))}
        </Table.Body>
      </Table>
    </div>
  );
};

export default ChangeHistoryList;
