import React from 'react';
import { Dimmer, Loader, Image, Segment } from 'semantic-ui-react';

const LoaderComponent = ({ isLoading }) => {
  if (isLoading) {
    return (
      <div className="loader">
        <Dimmer active>
          <Loader active inline="centered" indeterminate />
        </Dimmer>
      </div>
    );
  } else {
    return null;
  }
};

export default LoaderComponent;
