import React from 'react';
import { Container } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

const Navigation = ({ title, value }) => {
  return (
    <div className="top-navigation">
      <Container>
        <Link to="/">MYDANO Coach Monitoring</Link>
      </Container>
    </div>
  );
};

export default Navigation;
