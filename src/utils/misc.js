import axios from 'axios';
import * as moment from 'moment-timezone';

export const API = axios.create({
  baseURL: process.env.REACT_APP_SERVER_URL,
  responseType: 'json',
});

// '1300150018002000' -> ['13001500', '18002000']
// '13001500'-> '13:00-15:00'
export const timeParser = (str) => {
  if (!str) return '';
  let arr = [];

  for (let i = 0; i < str.length; i += 8) {
    arr.push(str.slice(i, i + 8));
  }

  let result = '';
  arr = arr.map((x, idx) => {
    let from = x.slice(0, 4);
    let to = x.slice(4);
    from = from.slice(0, 2) + ':' + from.slice(2);
    to = to.slice(0, 2) + ':' + to.slice(2);
    if (idx > 0) result += ', ';
    result += from + '-' + to;
  });
  return result;
};

export const labelColor = (programTypeId) =>
  programTypeId == 0 ? 'pink' : 'teal';

export const yesterdayDate = moment().add(-1, 'days').format('YYYY-MM-DD');

export const getProgramTypeStr = (programTypeId) =>
  programTypeId === 0 ? '토탈' : '베이직';

export const validateDate = (date) =>
  moment(date, 'YYYY-MM-DD', true).isValid();
