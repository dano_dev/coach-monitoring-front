import React, { useState } from 'react';
import './App.css';
import { Container } from 'semantic-ui-react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Navigation from './components/Navigation';
import Dashboard from './components/Dashboard';
import ViolatedList from './components/ViolatedList';
import UnassignedList from './components/UnassignedList';
import ChangeHistoryList from './components/ChangeHistoryList';

const App = () => {
  return (
    <Router>
      <div className="app">
        <header className="app-header">
          <Navigation />
        </header>
        <Container>
          <Switch>
            <Route exact path="/" component={Dashboard} />
            <Route path="/page-1" component={ViolatedList} />
            <Route path="/page-2" component={UnassignedList} />
            <Route path="/page-3" component={ChangeHistoryList} />
          </Switch>
        </Container>
      </div>
    </Router>
  );
};

export default App;
