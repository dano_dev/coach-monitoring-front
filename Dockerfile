FROM node:12 as builder

LABEL maintainer="jaegeon <zezaeoh@gmail.com>"

ARG BUILD_CMD=build
WORKDIR /app

COPY . .
RUN npm install && npm run ${BUILD_CMD}

FROM danodocker/nginx-env:1.17-slim as static

LABEL maintainer="jaegeon <jaegeon@dano.me>"

WORKDIR /usr/share/nginx/html
COPY --from=builder /app/build/ ./
